<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::group(['middleware' => 'web'], function () {
   // Auth::routes();
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('/admin/home', 'HomeController@index')->name('home');

    //schedule
    Route::get('/admin/schedule', 'ScheduleController@index');
    Route::get('/admin/add_schedule', 'ScheduleController@create');
    Route::post('/admin/schedule/create', 'ScheduleController@store');
    Route::get(' /admin/schedule/edit/{id}', 'ScheduleController@edit');
    Route::post('/admin/schedule/update/{id}', 'ScheduleController@update');
    Route::get(' /admin/schedule/delete/{id}', 'ScheduleController@destroy');


    //driver
    Route::get('/admin/driver', 'DriverController@index');
    Route::get('/admin/add_driver', 'DriverController@create');
    Route::post('/admin/driver/create', 'DriverController@store');
    Route::get(' /admin/driver/edit/{id}', 'DriverController@edit');
    Route::post('/admin/driver/update/{id}', 'DriverController@update');
    Route::get(' /admin/driver/delete/{id}', 'DriverController@destroy');


        //vechicle
    Route::get('/admin/vehicle', 'VehicleController@index');
    Route::get('/admin/add_vehicle', 'VehicleController@create');
    Route::post('/admin/vehicle/create', 'VehicleController@store');
    Route::get(' /admin/vehicle/edit/{id}', 'VehicleController@edit');
    Route::post('/admin/vehicle/update/{id}', 'VehicleController@update');
    Route::get(' /admin/vehicle/delete/{id}', 'VehicleController@destroy');

    //profile
    Route::get(' /admin/profile/{id}', 'ProfileController@show');
    
});