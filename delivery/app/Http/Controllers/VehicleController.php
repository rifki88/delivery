<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use Session;
use Validator;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Vehicle::orderBy('id','desc')->paginate(5);;
        return view('vehicle.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vehicle.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input,[
            'license_number' => 'required',
            'type' => 'required'
        ]);


        if($validator->fails()){
            return redirect('/admin/add_vehicle')->withInput()->withErrors($validator);

        }

        $foto = $request->file('photo');
        $nf = $foto->getClientOriginalName();
        $trim = str_replace(" ","",$nf);
        $exp = explode('.',$trim);
        $ext = end($exp);
        $nama_file = rand().'.'.$ext;
        $lokasi = $foto->getPathName();  
        $destination = 'vehicle/';
        $foto->move($destination,$nama_file);
        


        $vec = new Vehicle();
        $vec->license_number = $request->license_number;
        $vec->type = $request->type;
        $vec->capacity = $request->capacity;
        $vec->color = "blue";
        $vec->vehicle_name = $request->vehicle_name;
        $vec->photo = env('URL_DRIVER').$nama_file;
        $vec->specification = $request->specification;
        $vec->distribution = $request->distribution;
        $vec->save();
        return redirect('/admin/vehicle')->with('status', 'Vehicle created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Vehicle::findOrFail($id);
        return view('vehicle.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $foto = $request->file('photo');
        if(!empty($foto)){
        
        $nf = $foto->getClientOriginalName();
        $trim = str_replace(" ","",$nf);
        $exp = explode('.',$trim);
        $ext = end($exp);
        $nama_file = rand().'.'.$ext;
        $lokasi = $foto->getPathName();  
        $destination = 'vehicle/';
        $foto->move($destination,$nama_file);

        $vec = Vehicle::findOrFail($id);
        $vec->license_number = $request->license_number;
        $vec->type = $request->type;
        $vec->capacity = $request->capacity;
        $vec->color = "blue";
        $vec->vehicle_name = $request->vehicle_name;
        $vec->photo = env('URL_VEHICLE').$nama_file;
        $vec->specification = $request->specification;
        $vec->distribution = $request->distribution;
        $vec->save();

        }else{

        $vec = Vehicle::findOrFail($id);
        $vec->license_number = $request->license_number;
        $vec->type = $request->type;
        $vec->capacity = $request->capacity;
        $vec->color = "blue";
        $vec->vehicle_name = $request->vehicle_name;
        $vec->specification = $request->specification;
        $vec->distribution = $request->distribution;
        $vec->save();

        }
        return redirect('/admin/vehicle')->with('status', 'Vehicle updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Vehicle::findOrFail($id);
        $data->delete();
        return redirect('/admin/vehicle')->with('status', 'Vehicle Deleted!');
    }

    public function vehicleapi()
    {
        $data = Vehicle::all();
        return $data;
    }

}

