<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use Session;
use Validator;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Driver::orderBy('id','desc')->paginate(5);
        return view('driver.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('driver.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $validator = Validator::make($input,[
            'nin' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'photo' => 'required',
        ]);


        if($validator->fails()){
            return redirect('/admin/add_driver')->withInput()->withErrors($validator);
        }

        $foto = $request->file('photo');
        $nf = $foto->getClientOriginalName();
        $trim = str_replace(" ","",$nf);
        $exp = explode('.',$trim);
        $ext = end($exp);
        $nama_file = rand().'.'.$ext;
        $lokasi = $foto->getPathName();  
        $destination = 'driver/';
        $foto->move($destination,$nama_file);
        
        $driver = new Driver();
        $driver->nin = $request->nin;
        $driver->name = $request->name;
        $driver->phone = $request->phone;
        $driver->email = $request->email;
        $driver->address = $request->address;
        $driver->photo = env('URL_DRIVER').$nama_file;
        $driver->save();

        return redirect('/admin/driver')->with('status', 'Driver created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Driver::findOrFail($id);
        return view('driver.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $driver = Driver::findOrFail($id);

        
        if(!empty($request->file('photo'))){
        $foto = $request->file('photo');
        $nf = $foto->getClientOriginalName();
        $trim = str_replace(" ","",$nf);
        $exp = explode('.',$trim);
        $ext = end($exp);
        $nama_file = rand().'.'.$ext;
        $lokasi = $foto->getPathName();  
        $destination = 'driver/';
        $foto->move($destination,$nama_file);
        
        $driver->nin = $request->nin;
        $driver->name = $request->name;
        $driver->phone = $request->phone;
        $driver->email = $request->email;
        $driver->address = $request->address;
        $driver->photo = env('URL_DRIVER').$nama_file;
        $driver->save();

        }else{

        $driver->nin = $request->nin;
        $driver->name = $request->name;
        $driver->phone = $request->phone;
        $driver->email = $request->email;
        $driver->address = $request->address;
        $driver->save();

        }
        return redirect('/admin/driver')->with('status', 'Driver updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Driver::findOrFail($id);
        $data->delete();
        return redirect('/admin/driver')->with('status', 'Driver Deleted!');
    }

    public function driverapi()
    {
        $data = Driver::all();
        return $data;
    }
}
