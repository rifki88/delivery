<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedules;
use Session;
use Validator;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Schedules::orderBy('id','desc')->paginate(5);
        return view('schedule.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('schedule.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input,[
            'vanue_name' => 'required',
            'latlong' => 'required',
            'pic_name' => 'required',
            'pic_contact' => 'required',
            'date_and_time' => 'required',
            'recieved_report' => 'required'
        ]);


        if($validator->fails()){
            return redirect('/admin/add_schedule')->withInput()->withErrors($validator);

        }
        $schedule = new Schedules();
        $schedule->vanue_name = $request->vanue_name;
        $schedule->latlong = $request->latlong;
        $schedule->pic_name = $request->pic_name;
        $schedule->pic_contact = $request->pic_contact;
        $schedule->date_and_time = $request->date_and_time;
        $schedule->recieved_report = $request->recieved_report;
        $schedule->save();
        return redirect('/admin/schedule')->with('status', 'Schedule created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Schedules::findOrFail($id);
        return view('schedule.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $schedule = Schedules::findOrFail($id);
        $schedule->vanue_name = $request->vanue_name;
        $schedule->latlong = $request->latlong;
        $schedule->pic_name = $request->pic_name;
        $schedule->pic_contact = $request->pic_contact;
        $schedule->date_and_time = $request->date_and_time;
        $schedule->recieved_report = $request->recieved_report;
        $schedule->save();
        return redirect('/admin/schedule')->with('status', 'Schedule Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Schedules::findOrFail($id);
        $data->delete();
        return redirect('/admin/schedule')->with('status', 'Schedule Deleted!');
    }

    public function scheduleapi()
    {
        
        $data = Schedules::all();
        return $data;
    }


}
