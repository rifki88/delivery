@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">      
             <div class="col-md-12" style="background-color:#fff">
                    <div class="row">
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="margin-left:10px;" class="btn btn-primary">Add Schedule</div>
                        </div>
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="text-align:right; margin-right:10px;"> <a href="/admin/schedule" class="btn btn-primary">Back</a></div>
                        </div>
                    </div>    
                </div>

             <div class="card-body">     
                     
              @if ($errors->any())
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong> All Field </strong> is Required
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                @endif

                
                <form method="POST" action="/admin/schedule/create">
                {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                        <label for="vanue_name">Vanue Name</label>
                        <input type="text" class="form-control" id="vanue_name" name="vanue_name">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="latlong">Latlong</label>
                        <input type="text" class="form-control" id="latlong" name="latlong">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="pic_name">Pic Name</label>
                            <input type="text" class="form-control" id="pic_name" name="pic_name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="pic_contact">Pic Contact</label>
                            <input type="text" class="form-control" id="pic_contact" name="pic_contact">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="date_and_time">Date And Time</label>
                            <input type="text" class="form-control" id="date_and_time" name="date_and_time">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="recieved_report">Recieved Report</label>
                            <input type="text" class="form-control" id="recieved_report" name="recieved_report">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"> Create </button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection