@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="col-md-12" style="background-color:#fff">
                    <div class="row">
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="margin-left:10px;" class="btn btn-primary">Schedule</div>
                        </div>
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="text-align:right; margin-right:10px;"> <a href="/admin/add_schedule" class="btn btn-primary">Add +</a></div>
                        </div>
                    </div>    
                </div>
                
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                    @endif

                  
                    <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Vanue Name</th>
                            <th scope="col">Lat Long</th>
                            <th scope="col">Pic Name</th>
                            <th scope="col">Pic Contact</th>
                            <th scope="col">Date And Time</th>
                            <th scope="col">Recieved Report</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>     
                        @foreach($data as $row)
                            <tr>
                                <th scope="row">#s{{$row->id}}</th>
                                <td>{{$row->vanue_name}}</td>
                                <td>{{$row->latlong}}</td>
                                <td>{{$row->pic_name}}</td>
                                <td>{{$row->pic_contact}}</td>
                                <td>{{$row->date_and_time}}</td>
                                <td>{{$row->recieved_report}}</td>
                                <td><a href="/admin/schedule/edit/{{$row->id}}" class="btn btn-warning">Edit</a></td>
                                <td><a href="/admin/schedule/delete/{{$row->id}}" class="btn btn-danger" onclick="return confirm('Are You Sure Want to Delete this {{$row->vanue_name}} ')">Delete</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>                    
                    <div class="paging">
                        {{$data->links()}}    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
