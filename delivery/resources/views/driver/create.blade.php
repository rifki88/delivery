@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">      
             <div class="col-md-12" style="background-color:#fff">
                    <div class="row">
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="margin-left:10px;" class="btn btn-primary">Add Driver</div>
                        </div>
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="text-align:right; margin-right:10px;"> <a href="/admin/driver" class="btn btn-primary">Back</a></div>
                        </div>
                    </div>    
                </div>

             <div class="card-body">          
                <form method="POST" action="/admin/driver/create" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                        <label for="nin">NiK</label>
                        <input type="text" class="form-control" id="nin" name="nin">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="phone_number">Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="phone">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address" name="address">
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-12 text-left">
                            <img id="output" width="250"/>
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="photo">Photo</label>
                            <input type="file" class="form-control" id="photo" name="photo" onchange="loadFile(event)">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary"> Create </button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection


<script>        
    function loadFile(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
};
</script>