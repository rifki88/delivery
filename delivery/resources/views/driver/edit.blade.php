@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">      
             <div class="col-md-12" style="background-color:#fff">
                    <div class="row">
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="margin-left:10px;" class="btn btn-primary">Edit Driver</div>
                        </div>
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="text-align:right; margin-right:10px;"> <a href="/admin/driver" class="btn btn-primary">Back</a></div>
                        </div>
                    </div>    
                </div>

             <div class="card-body">          
                <form method="POST" action="/admin/driver/update/{{$data->id}}" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                        <label for="nin">NiK</label>
                        <input type="text" class="form-control" id="nin" name="nin" value="{{$data->nin}}">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$data->name}}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="phone_number">Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{$data->phone}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{$data->email}}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address" name="address" value="{{$data->address}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            @if($data->photo == "")
                              &nbsp; No Image
                            @else
                            <img src="{{$data->photo}}" width="250">
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="file">Photo</label>
                            <input type="file" class="form-control" id="file" name="photo">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"> Update </button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection