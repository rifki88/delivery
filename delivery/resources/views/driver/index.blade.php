@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="col-md-12" style="background-color:#fff">
                    <div class="row">
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="margin-left:10px;" class="btn btn-primary">Driver</div>
                        </div>
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="text-align:right; margin-right:10px;"> <a href="/admin/add_driver" class="btn btn-primary">Add +</a></div>
                        </div>
                    </div>    
                </div>
                
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                  
                    <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nik</th>
                            <th scope="col">Name</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Email</th>
                            <th scope="col">Address</th>
                            <th scope="col">Foto</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        @foreach($data as $row)
                            <tr>
                                <th scope="row">#d{{$row->id}}</th>
                                <td>{{$row->nin}}</td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->phone}}</td>
                                <td>{{$row->email}}</td>
                                <td>{{$row->address}}</td>
                                <td>
                                @if(!empty($row->photo))
                                    <img src="{{$row->photo}}" width="100">
                                @else
                                    No Image
                                @endif
                                </td>
                                <td><a href="/admin/driver/edit/{{$row->id}}" class="btn btn-warning">Edit</a></td>
                                <td><a href="/admin/driver/delete/{{$row->id}}" class="btn btn-danger" onclick="return confirm('Are You Sure Want to Delete this {{$row->name}} ')">Delete</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                        </table>

                    </div>

                    <div class="paging">
                        {{$data->links()}}    
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
