@extends('layouts.app')

@section('content')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Work', 8],
  ['Eat', 2],
  ['TV', 4],
  ['Gym', 2],
  ['Sleep', 8]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'My Average Day', 'width':550, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart1 = new google.visualization.PieChart(document.getElementById('piechart1'));
  var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
  var chart3 = new google.visualization.PieChart(document.getElementById('piechart3'));
  var chart4 = new google.visualization.PieChart(document.getElementById('piechart4'));
  chart1.draw(data, options);
  chart2.draw(data, options);
  chart3.draw(data, options);
  chart4.draw(data, options);
}
</script>


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
           
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                <div class="card-body" style="background-color: #fff">
                    <h5 class="card-title text-center">Chart 1</h5>
                    <div style="overflow:hidden" id="piechart1"></div>
                </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                <div class="card-body" style="background-color: #fff">
                    <h5 class="card-title text-center">Chart 2</h5>
                    <div style="overflow:hidden" id="piechart2"></div>
                </div>
                </div>
            </div>
        </div>

        <br>
            
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                <div class="card-body" style="background-color: #fff">
                    <h5 class="card-title text-center">Chart 3</h5>
                    <div style="overflow:hidden" id="piechart3"></div>
                </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                <div class="card-body" style="background-color: #fff">
                    <h5 class="card-title text-center">Chart 4</h5>
                    <div style="overflow:hidden" id="piechart4"></div>
                </div>
                </div>
            </div>
        </div>

        

        </div>
    </div>
</div>
@endsection
