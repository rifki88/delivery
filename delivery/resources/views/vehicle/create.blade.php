@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">      
             <div class="col-md-12" style="background-color:#fff">
                    <div class="row">
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="margin-left:10px;" class="btn btn-primary">Add Vehicle</div>
                        </div>
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="text-align:right; margin-right:10px;"> <a href="/admin/vehicle" class="btn btn-primary"> Back</a></div>
                        </div>
                    </div>    
                </div>

             <div class="card-body">          

             @if ($errors->any())
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong> All Field </strong> is Required
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                @endif


                <form method="POST" action="/admin/vehicle/create" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                        <label for="license_name">License Number</label>
                        <input type="text" class="form-control" id="license_number" name="license_number">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="type">Type</label>
                        <select class="form-control" id="type" name="type">
                            <option value="vip">VIP</option>
                            <option value="regular">Regular</option>    
                        </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="capacity"> Capacity </label>
                            <input type="text" class="form-control" id="capacity" name="capacity">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="vehicle_name"> Vehicle Name </label>
                            <input type="text" class="form-control" id="vehicle_name" name="vehicle_name">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="photo"> Photo </label>
                            <input type="file" class="form-control" id="photo" name="photo">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="specification"> Specification </label>
                            <input type="text" class="form-control" id="specification" name="specification">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="distribution"> Distribution </label>
                            <textarea class="form-control" id="distribution" name="distribution" style="height:300px"></textarea>
                        </div>
                    </div>


                    <button type="submit" class="btn btn-primary"> Create </button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection