@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="col-md-12" style="background-color:#fff">
                    <div class="row">
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="margin-left:10px;" class="btn btn-primary">Vehicle</div>
                        </div>
                        <div class="col-md-6" style="background-color:#fff; padding: 10px;">
                            <div style="text-align:right; margin-right:10px;"> <a href="/admin/add_vehicle" class="btn btn-primary">Add +</a></div>
                        </div>
                    </div>    
                </div>
                
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                  
                    <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Vehicle Name</th>
                            <th scope="col">License Number</th>
                            <th scope="col">Type</th>
                            <th scope="col">Color</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        @foreach($data as $row)
                            <tr>
                                <th scope="row">#v{{$row->id}}</th>
                                <td>{{$row->vehicle_name}}</td>
                                <td>{{$row->license_number}}</td>
                                <td>{{$row->type}}</td>
                                <td>{{$row->color}}</td>
                                <td><a href="/admin/vehicle/edit/{{$row->id}}" class="btn btn-warning">Edit</a></td>
                                <td><a href="/admin/vehicle/delete/{{$row->id}}" class="btn btn-danger" onclick="return confirm('Are You Sure Want to Delete this {{$row->license_number}} ')">Delete</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                        </table>

                    </div>                    
                    <div class="paging">
                        {{$data->links()}}    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
